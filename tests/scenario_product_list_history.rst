=============================
Product List History Scenario
=============================

Imports::

    >>> from proteus import Model, Wizard
    >>> from decimal import Decimal
    >>> from datetime import datetime
    >>> from trytond.tests.tools import activate_modules

Install product_list_history::

    >>> config = activate_modules('product_list_history')


Create Product::

    >>> ProductUom = Model.get('product.uom')
    >>> ProductTemplate = Model.get('product.template')
    >>> Product = Model.get('product.product')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> product = Product()
    >>> template = ProductTemplate()
    >>> template.name = 'Product'
    >>> template.default_uom = unit
    >>> template.type = 'goods'
    >>> template.list_price = Decimal('20')
    >>> template.cost_price = Decimal('8')
    >>> template.save()
    >>> product.template = template
    >>> product.save()


Update List price::

    >>> template.list_price = Decimal('10')
    >>> template.save()
    >>> template.list_price = Decimal('11')
    >>> template.save()
    >>> template.list_price = Decimal('21')
    >>> template.save()
    >>> template.list_price = Decimal('22')
    >>> template.save()


Test List Price History::

    >>> History = Model.get('product.product.list_history')
    >>> history = History.find([])
    >>> len(history)
    5
    >>> history[0].list_price
    Decimal('22')
    >>> history[1].list_price
    Decimal('21')
    >>> history[2].list_price
    Decimal('11')
    >>> history[3].list_price
    Decimal('10')
    >>> history[4].list_price
    Decimal('20')
