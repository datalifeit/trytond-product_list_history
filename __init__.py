# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from . import product


def register():
    Pool.register(
        product.ProductListPrice,
        product.ProductListHistory,
        module='product_list_history', type_='model')
    Pool.register(
        product.OpenProductListHistory,
        module='product_list_history', type_='wizard')
